from django.db import models

# Create your models here.
from django.utils.safestring import mark_safe


class Category(models.Model):
    name = models.CharField(max_length=200, help_text='entrez le nom de la catégorie')
    image_url = models.ImageField(upload_to='category_images/', help_text="uploadez votre image",
                                  verbose_name="Image", null=True)

    class Meta:
        verbose_name_plural = "Categories"
        ordering = ['name']

    def __str__(self):
        return self.name




class Article(models.Model):
    title = models.CharField(max_length=200, help_text="entrez le titre de l'article", verbose_name="titre")
    summary = models.CharField(max_length=2000, help_text='entrez le résumé', verbose_name="resumé")
    content = models.TextField(help_text='entrez le contenu', verbose_name="contenu")
    slug = models.SlugField(max_length=200, help_text="entrez le l'url de l'article", null=True, blank=True, default="")
    cover_image = models.ImageField(upload_to='image', help_text='Enter an image url for the article', null=True,)

    thumbnail_image_url = models.CharField(max_length=2048, help_text='Enter a thumbnail url for the article', null=True, default='')
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    publish_time = models.DateTimeField(null=True, help_text='Publication date of the article')
    visibily = models.BooleanField(default=False,  help_text="Visibilté de l'article")
    file = models.FileField(upload_to='files/',help_text="ajouter un fichier à l'article", null=True, blank=True)

    category = models.ForeignKey(Category, help_text="choisir votre categorie", on_delete=models.RESTRICT, null=True,
                                 default='', blank=True, related_name='articles')


    @property
    def image_preview(self):
        if self.cover_image:
            return mark_safe(f'<img src"{self.cover_image.url}" width="300" height="200" />')
        return ""

    def __str__(self):
        return self.title


class Comment(models.Model):
    author_name = models.CharField(max_length=200, help_text="entrez l'auteur de l'article", blank=True)
    content = models.TextField(max_length=2000, help_text='entrez le contenu')
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    article = models.ForeignKey(Article, help_text="choisir votre article", on_delete=models.SET_NULL, null=True,
                                 default='', blank=True, related_name='comments')


    def __str__(self):
        return self.content